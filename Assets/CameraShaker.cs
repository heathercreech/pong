using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class CameraShaker : MonoBehaviour {
    [SerializeField]
    bool isShakeEnabled = true;

    CinemachineVirtualCamera cinemachineCamera;
    CinemachineBasicMultiChannelPerlin shakeNoise;

    float timeLeft = 0;

    [SerializeField]
    float shakeIntensity = 1.0f;

    [SerializeField]
    float shakeDuration = 0.1f;

    void Start () {
        cinemachineCamera = GetComponent<CinemachineVirtualCamera> ();
        shakeNoise = cinemachineCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin> ();
    }

    void Update () {
        if (timeLeft > 0) {
            timeLeft = Mathf.Max (timeLeft - Time.deltaTime, 0);
            if (timeLeft == 0) {
                shakeNoise.m_AmplitudeGain = 0;
            }
        }
    }

    public void Shake () {
        if (!isShakeEnabled) {
            return;
        }

        shakeNoise.m_AmplitudeGain = shakeIntensity;
        timeLeft = shakeDuration;
    }

    public void ShakeWithIntensity(float intensity) {
        if (!isShakeEnabled) {
            return;
        }

        shakeNoise.m_AmplitudeGain = intensity;
        timeLeft = shakeDuration;
    }
}