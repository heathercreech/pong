using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerPaddleMovement : MonoBehaviour {

  float inputAxis = 0.0f;

  [SerializeField]
  float moveSpeed = 10.0f;

  [SerializeField]
  float maxYPosition = 6.0f;

  [SerializeField]
  float minYPosition = -6.0f;

  void FixedUpdate () {
    if (inputAxis != 0.0f) {

      float deltaY = inputAxis * Time.deltaTime * moveSpeed;

      if (deltaY + transform.position.y > maxYPosition) {
        transform.position = new Vector3 (transform.position.x, maxYPosition, 0);
      } else if (deltaY + transform.position.y < minYPosition) {
        transform.position = new Vector3 (transform.position.x, minYPosition, 0);
      } else {
        Vector3 deltaPosition = new Vector3 (0, deltaY, 0);
        transform.position = transform.position + deltaPosition;
      }

    }
  }

  public void OnMove (InputAction.CallbackContext context) {
    inputAxis = context.ReadValue<float> ();
  }
}