using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Rigidbody2D))]
[RequireComponent (typeof (AudioSource))]
public class BallAudio : MonoBehaviour {

    [System.Serializable]
    struct GameObjectToClip {
        public GameObject gameObject;
        public AudioClip clip;
    }

    [SerializeField]
    List<GameObjectToClip> gameObjectsToClips = new List<GameObjectToClip> ();

    Dictionary<GameObject, AudioClip> gameObjectsToClipDictionary = new Dictionary<GameObject, AudioClip> ();

    AudioSource source;

    // Start is called before the first frame update
    void Start () {
        source = GetComponent<AudioSource> ();
        foreach (var mapObject in gameObjectsToClips) {
            gameObjectsToClipDictionary.Add (mapObject.gameObject, mapObject.clip);
        }
    }

    // Update is called once per frame
    void Update () {

    }

    private void OnCollisionEnter2D (Collision2D other) {
        if (gameObjectsToClipDictionary.ContainsKey (other.gameObject)) {
            source.PlayOneShot (gameObjectsToClipDictionary[other.gameObject]);
        }
    }
}