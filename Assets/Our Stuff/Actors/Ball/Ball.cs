using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class BounceEvent : UnityEvent<float> { }

public class Ball : MonoBehaviour, IRespawnable {

    [SerializeField]
    float moveSpeed = 8.0f;

    new Rigidbody2D rigidbody;
    Animator animator;
    SpriteRenderer spriteRenderer;
    Sprite initialSprite;
    [SerializeField]
    Sprite bounceLeftSprite;
    [SerializeField]
    Sprite bounceRightSprite;

    [SerializeField]
    BounceEvent onBounce = new BounceEvent ();

    [SerializeField]
    float bounceDuration = 0.2f;
    bool isBouncing = false;
    float currentBounceDuration = 0.0f;

    Vector2 bounceDirection;

    void Start () {
        rigidbody = GetComponent<Rigidbody2D> ();
        animator = GetComponent<Animator> ();
        spriteRenderer = GetComponent<SpriteRenderer> ();
        initialSprite = spriteRenderer.sprite;
        ApplyDirectionChange (ChooseDirection ());
    }

    void OnCollisionEnter2D (Collision2D other) {

        Paddle paddle = other.gameObject.GetComponent<Paddle> ();
        if (paddle != null && other.contacts.Length > 0) {
            ContactPoint2D firstContact = other.contacts[0];
            Debug.DrawRay (firstContact.point, firstContact.normal, Color.magenta, 1.0f);

            if (Vector3.Dot (firstContact.normal, Vector3.up) >.75 || Vector3.Dot (firstContact.normal, Vector3.down) >.75) {
                Debug.Log ("We hit the bottom");
            } else {
                float percent = paddle.PercentOfPaddleHeight (firstContact.point);
                Debug.DrawRay (firstContact.point, GetBounceDirection (firstContact.normal.x, percent), Color.white, 1.0f);
                if (Vector3.Dot (firstContact.normal, Vector3.right) >.75) {
                    spriteRenderer.sprite = bounceRightSprite;
                } else {
                    spriteRenderer.sprite = bounceLeftSprite;
                }
                isBouncing = true;
                bounceDirection = GetBounceDirection (firstContact.normal.x, percent);
                rigidbody.velocity = Vector2.zero;
            }
        }

        onBounce.Invoke(transform.localScale.x);
    }

    void Update () {
        if (isBouncing && currentBounceDuration >= bounceDuration) {
            spriteRenderer.sprite = initialSprite;
            currentBounceDuration = 0;
            isBouncing = false;
            ApplyDirectionChange (bounceDirection);
        } else if (isBouncing) {
            currentBounceDuration += Time.deltaTime;
        }
    }

    Vector2 GetBounceDirection (float xDirection, float percent) {
        float yDirection = Mathf.Lerp (-1, 1, percent);
        return new Vector2 (xDirection, yDirection);
    }

    void ApplyDirectionChange (Vector2 direction) {
        rigidbody.velocity = direction * moveSpeed;
    }

    Vector2 ChooseDirection () {
        int randomNumber = Random.Range (0, 2);

        if (randomNumber == 0) return new Vector2 (-1, 0);
        else return new Vector2 (1, 0);
    }

    public void Respawn () {
        transform.position = Vector3.zero;
        ApplyDirectionChange (ChooseDirection ());
    }
}