using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallCameraShaker : MonoBehaviour
{

    [SerializeField]
    CameraShaker cameraShaker;

    private void OnCollisionEnter2D(Collision2D other) {
        if (cameraShaker != null) {
            cameraShaker.ShakeWithIntensity(transform.localScale.x);
        }
    }
}
