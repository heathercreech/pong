using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ScoreZone : MonoBehaviour {

    [SerializeField]
    UnityEvent onScoreZoneEnter = new UnityEvent ();
    ParticleSystem[] particles;
    AudioSource audioSource;

    // Start is called before the first frame update
    void Start () {
        particles = GetComponentsInChildren<ParticleSystem> ();
        audioSource = GetComponentInChildren<AudioSource> ();
    }

    // Update is called once per frame
    void Update () {

    }

    private void OnTriggerEnter2D (Collider2D other) {
        IRespawnable respawnableObject = other.gameObject.GetComponent<IRespawnable> ();
        if (respawnableObject != null) {
            PlayExplosion (respawnableObject.gameObject.transform.position);
            respawnableObject.Respawn ();
            onScoreZoneEnter.Invoke ();
        }
    }

    public void PlayExplosion (Vector3 playPosition) {
        foreach (ParticleSystem particleSystem in particles) {
            particleSystem.transform.position = playPosition;
            particleSystem.Play ();
        }
        audioSource.transform.position = playPosition;
        audioSource.Play ();
    }
}