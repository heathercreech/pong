using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follower : MonoBehaviour {
    [SerializeField]
    GameObject leader;

    [SerializeField]
    float followerSpeed = 5f;

    [SerializeField]
    float maxYPosition = 6.0f;

    [SerializeField]
    float minYPosition = -6.0f;

    // Start is called before the first frame update
    void Start () {

    }

    private Vector3 predictedPosition (Vector3 targetPosition, Vector3 leaderPosition, Vector3 targetVelocity, float leaderSpeed) {
        Vector3 displacement = targetPosition - leaderPosition;
        float targetMoveAngle = Vector3.Angle (-displacement, targetVelocity) * Mathf.Deg2Rad;
        // if the target is stopping or if it is impossible for the projectile to catch up with the target (Sine Formula)
        if (targetVelocity.magnitude == 0 || targetVelocity.magnitude > leaderSpeed && Mathf.Sin (targetMoveAngle) / leaderSpeed > Mathf.Cos (targetMoveAngle) / targetVelocity.magnitude) {
            Debug.Log ("Position prediction is not feasible.");
            return targetPosition;
        }

        // also Sine Formula
        float travelAngle = Mathf.Asin (Mathf.Sin (targetMoveAngle) * targetVelocity.magnitude / leaderSpeed);
        return targetPosition + targetVelocity * displacement.magnitude / Mathf.Sin (Mathf.PI - targetMoveAngle - travelAngle) * Mathf.Sin (travelAngle) / targetVelocity.magnitude;
    }

    // Update is called once per frame
    void Update () {
        if (leader != null) {
            Vector2 leaderVelocity = leader.GetComponent<Rigidbody2D> ().velocity;
            Vector3 targetPosition;
            float offset = Random.Range (-1, 1);

            if (Vector2.Dot (Vector2.right, leaderVelocity.normalized) > 0) {
                Vector3 expectedPosition = predictedPosition (leader.transform.position, transform.position, leaderVelocity, Vector2.SqrMagnitude (leaderVelocity));
                targetPosition = new Vector3 (transform.position.x, expectedPosition.y + offset, transform.position.z);
            } else {
                targetPosition = new Vector3 (transform.position.x, 0, transform.position.z);
            }

            if (targetPosition.y < minYPosition) {
                targetPosition.y = minYPosition;
            } else if (targetPosition.y > maxYPosition) {
                targetPosition.y = maxYPosition;
            }

            float distanceToMove = Time.deltaTime * followerSpeed;
            transform.position = Vector3.MoveTowards (transform.position, targetPosition, distanceToMove);

        }

    }
}