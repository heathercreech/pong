using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class GameQuiter : MonoBehaviour
{
    [SerializeField]
    UnityEvent OnQuitGame = new UnityEvent();

    public void QuitGame  ()
    {
        OnQuitGame.Invoke();

        Application.Quit();
    }


}
