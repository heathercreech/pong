using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ActivePowerUps : MonoBehaviour {
    [SerializeField]
    Dictionary<string, PowerUp> powerUps = new Dictionary<string, PowerUp> ();

    [SerializeField]
    PowerUpContext context;

    public void AddPowerUp (PowerUp powerUp) {
        powerUps.Add (powerUp.Name, powerUp);
        powerUp.OnPickup (context);
    }

    public void HandleBounce() {
        foreach (PowerUp powerUp in powerUps.Values) {
            powerUp.OnBallBounce(context);
        }
    }

    public void RemovePowerUp (PowerUp powerUp) {
        powerUps.Remove (powerUp.Name);
        powerUp.OnRemove (context);
    }

}