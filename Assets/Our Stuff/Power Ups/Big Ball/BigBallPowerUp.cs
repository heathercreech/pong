using System;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu (menuName = "PowerUps/Big Ball")]
public class BigBallPowerUp : PowerUp {

    [SerializeField]
    float maxSize = 10.0f;

    public BigBallPowerUp () : base ("Big Bcall") { }

    public override void OnPickup (PowerUpContext context) {
        context.ball.gameObject.transform.localScale += Vector3.one;
    }

    public override void OnBallBounce(PowerUpContext context) {
        context.ball.gameObject.transform.localScale += Vector3.one;
        if (context.ball.gameObject.transform.localScale.x > maxSize) {
            context.ball.gameObject.transform.localScale = Vector3.one * maxSize;
        }
    }

    public override void OnRemove (PowerUpContext context) {
        context.ball.gameObject.transform.localScale -= Vector3.one;
    }
}