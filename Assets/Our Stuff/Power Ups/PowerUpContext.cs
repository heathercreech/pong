using UnityEngine;

public class PowerUpContext : MonoBehaviour {

    public Ball ball;
    public Paddle playerPaddle;
    public Paddle aiPaddle;
    public ScoreKeeper score;
}