using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class PowerUpEvent : UnityEvent<PowerUp> { }

public class PowerUpPickUp : MonoBehaviour {

    [SerializeField]
    PowerUp powerUp;

    [SerializeField]
    PowerUpEvent OnPickupEvent;

    public void OnPickup () {
        OnPickupEvent.Invoke (powerUp);
        Destroy (this.gameObject);
    }

    private void OnCollisionEnter2D (Collision2D other) {
        Paddle paddle = other.gameObject.GetComponent<Paddle> ();
        // Only players can pick up powerups for the time being.
        if (paddle.affiliation == Paddle.PaddleAffiliation.Player) {
            OnPickup ();
        }
    }
}