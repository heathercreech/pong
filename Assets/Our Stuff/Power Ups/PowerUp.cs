using System;
using UnityEngine;

[System.Serializable]
public abstract class PowerUp : ScriptableObject {

  private string powerUpName;

  public virtual void OnPickup (PowerUpContext context) { }
  public virtual void OnBallBounce (PowerUpContext context) { }
  public virtual void OnPaddleHit (PowerUpContext context) { }
  public virtual void OnRemove (PowerUpContext context) { }

  public string Name { get { return powerUpName; } private set { powerUpName = value; } }

  public PowerUp (string name) {
    Name = name;
  }
}