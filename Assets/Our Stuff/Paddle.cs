using UnityEngine;

public class Paddle : MonoBehaviour {

    public enum PaddleAffiliation {
        Player,
        Ai
    }

    public PaddleAffiliation affiliation;
    public float width { get { return transform.localScale.x; } }
    public float height { get { return transform.localScale.y; } }

    public float PercentOfPaddleHeight (Vector2 worldPosition) {
        float maxY = transform.position.y + this.height / 2;
        float minY = transform.position.y - this.height / 2;

        return (worldPosition.y - minY) / (maxY - minY);
    }
}