using UnityEngine;

public interface IRespawnable {
  GameObject gameObject { get; }
  public void Respawn ();
}