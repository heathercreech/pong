using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class ScoreUI : MonoBehaviour {

    [SerializeField]
    GameObject playerTextObject;
    TextMeshProUGUI playerText;

    [SerializeField]
    GameObject aiTextObject;
    TextMeshProUGUI aiText;

    void Start () {
        playerText = playerTextObject.GetComponent<TextMeshProUGUI> ();
        aiText = aiTextObject.GetComponent<TextMeshProUGUI> ();

        playerText.text = "0";
        aiText.text = "0";
    }

    public void UpdatePlayerScoreText (int score) {
        playerText.text = score.ToString ();
    }

    public void UpdateAiScoreText (int score) {
        aiText.text = score.ToString ();
    }

}