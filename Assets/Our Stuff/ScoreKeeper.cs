using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ScoreKeeper : MonoBehaviour {

    [SerializeField]
    UnityEvent<int> OnAiScoreChange = new UnityEvent<int> ();

    [SerializeField]
    UnityEvent<int> OnPlayerScoreChange = new UnityEvent<int> ();

    [SerializeField]
    UnityEvent OnWin = new UnityEvent ();

    [SerializeField]
    UnityEvent OnLose = new UnityEvent ();

    int playerScore = 0;
    int aiScore = 0;

    void SetPlayerScore (int value) {
        playerScore = value;
        OnPlayerScoreChange.Invoke (value);
    }

    void SetAiScore (int value) {
        aiScore = value;
        OnAiScoreChange.Invoke (value);
    }

    public void IncrementPlayerScore () {
        SetPlayerScore (playerScore + 1);
        CheckScores ();
    }

    public void IncrementAIScore () {
        SetAiScore (aiScore + 1);
        CheckScores ();
    }

    public void ResetScores () {
        SetPlayerScore (0);
        SetAiScore (0);
    }

    public void CheckScores () {
        if (playerScore >= 3) OnWin.Invoke ();
        else if (aiScore >= 3) OnLose.Invoke ();
    }
}