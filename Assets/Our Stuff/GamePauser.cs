using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class GamePauser : MonoBehaviour {

    [SerializeField]
    UnityEvent OnPause = new UnityEvent ();
    [SerializeField]
    UnityEvent OnResume = new UnityEvent ();

    public void Pause () {
        Time.timeScale = 0;
        OnPause.Invoke ();
    }

    public void PauseWithoutEffects () {
        Time.timeScale = 0;
    }

    public void Resume () {
        Time.timeScale = 1;
        OnResume.Invoke ();
    }

    public void TogglePause () {
        if (Time.timeScale == 0) {
            Resume ();
        } else {
            Pause ();
        }
    }

    public void OnPauseGame () {
        TogglePause ();
    }
}